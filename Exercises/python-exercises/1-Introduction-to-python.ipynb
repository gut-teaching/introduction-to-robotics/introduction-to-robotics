{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python\n",
    "\n",
    "**Learning Objectives**\n",
    "\n",
    "In this notebook we will look at the key concepts in programming using the programming language `Python`. After this exercise, you will know ...\n",
    "> - common programming languages used in robotics and their use cases.\n",
    "> - Pythons basic syntax, variables, data types, programming constructs and modules.\n",
    "> - how to write a Python function.\n",
    "> - how to import and use modules (also called libraries).\n",
    "\n",
    "## Programming in robotics\n",
    "\n",
    "Per **definition** \"a robot is an autonomous machine capable of sensing its environment, carrying out computations to make decisions, and performing actions in the real world\" ([IEEE](https://robots.ieee.org/learn/what-is-a-robot/)). \n",
    "\n",
    "The main **hardware components** of robots are *actuators*, *sensors* and a *computational unit*. These three hardware building blocks are needed in order to make a robot that corresponds to the definition running.\n",
    "But another essential part to make a robot running is its **software**. Programs have the task to **process** the sensed data and **interpret** it, **take decisions** and make the actuators act accordingly.\n",
    "\n",
    "So robot programming is basically programming the computational unit inside a robot to perform for a defined use case using its actuators and information/feedback from the sensors.\n",
    "The computational unit that runs the software/programs can be seen as the brain of the robot. Programming is therefore of great importance in robotics.\n",
    "\n",
    "## Programming languages\n",
    "\n",
    "In robotics many different programming languages are used. Depending on the **focus** of the current robotic project, the different programming languages have advantages and disadvantages. \n",
    "\n",
    "For projects or tasks where **computation time** is relevant or **low-level programming of hardware** is desired usually `C/C++` is used. \n",
    "\n",
    "`Python` is commonly used, when the focus is on **rapid prototyping** or when the goal is **learning robotics programming** with not much background in programming. `Python` is a beginner friendly programming language, as it has an easily readable language, comes with a lot of ready-to-use libraries and is a garbage collecting language, which means that the user doesn't have to take care of memory usage and management.\n",
    "\n",
    "In scientific research the language `Matlab` is also sometimes used because it's orientation towards **mathematical programming**. That makes it easy to test complex algorithm with less lines of code.\n",
    "\n",
    "During the `Introduction to Robotics` course we will use `Python` since it's well suited for **beginners** and for **rapid prototyping**.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python\n",
    "\n",
    "We will continue now with an introduction to the basic programming constructs in `Python`. The programming constructs are similar in different languages, so once you learned them in `Python` it will be easier to learn new languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variables\n",
    "\n",
    "**Variables** can be described as containers for storing data values.\n",
    "\n",
    "Variable creation works in `Python` by just assigning a value to it. There is **no need in `Python` to declare a variables before using them or declare their type**. That's why it's called a **dynamically typed language**.\n",
    "\n",
    "The naming convention in `Python` is to use lowercase single letter, word or words. If several words should be used, seperate them with an underscore."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "course = \"Intro-to-robotics\" # String (str)\n",
    "x = 5   #x is an int variable\n",
    "y = 5.0 #y is a float variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Types\n",
    "\n",
    "Like mentioned in the **Variables** subsection, values assigned to variables have a certain **data type**. `Python` comes with a few built-in data types. The most important ones we will use are listed below:\n",
    "\n",
    "- data type for **text**: *str*\n",
    "- data types for **numeric** values: *int*, *float*, *complex*\n",
    "- data types for **sequences**: *list*, *tuple*\n",
    "- data type for **mapping**: *dict*\n",
    "- data type for **Boolean** values: *bool*\n",
    "\n",
    "To find out the data type of a certain variable we can uuse the `type()` function. `print` is another commonly used function to print something to the terminal. There are several different ways to use the `print` function. So, please pay attention to the way it is used to learn different methods.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Type of 'x' is  <class 'int'>\n",
      "Type of 'y' is  <class 'float'>\n",
      "Type of 'z' is  <class 'str'>\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "print(\"Type of 'x' is \", type(x)) # prints <class 'int'>\n",
    "y = 2.0\n",
    "print(\"Type of 'y' is \", type(y)) # prints <class 'float'>\n",
    "z = \"text\"\n",
    "print(\"Type of 'z' is \", type(z)) # prints <class 'str'>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Beside these built-in types one can define as well own data types. But more information on that in following appointments.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operators\n",
    "\n",
    "### Arithmetic operators\n",
    "If a variable has a numeric data type it's possible to use common mathematical operators on it. A table of all built-in operators can be found [here](https://docs.python.org/3/library/operator.html#mapping-operators-to-functions).\n",
    "\n",
    "Here a few examples:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 1.0\n",
    "y = 2.0\n",
    "print(f\"Addition {x+y}\") # printed result would be 3\n",
    "print(\"Subtraction {}\".format(x-y)) # printed result would be -1\n",
    "print(\"Multiplication: {} Division: {}\".format(x*y , x/y)) # printed result would be 2 and 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relational & logical operators\n",
    "\n",
    "**Relational** and **logical operators** are needed in nearly every programme to mange the flow of the program. They are used to define **conditions** on which **decision structures** are then based.\n",
    "\n",
    "**Relational operators**:\n",
    "\n",
    "- define a relationship between to operands\n",
    "- return a boolean value (True or False)\n",
    "- Operators:\n",
    "    - \\> (greater than)\n",
    "    - \\< (less than)\n",
    "    - == (equal to)\n",
    "    - != (not equal to)\n",
    "    - \\>= (greater than or equal to)\n",
    "    - \\<= (less than or equal to)\n",
    "\n",
    "**Logical operators**:\n",
    "\n",
    "- used in expressions where the operands are of type bool (so True or False)\n",
    "- returns a boolean value (True or False)\n",
    "- Operators:\n",
    "    - AND\n",
    "    - OR\n",
    "    - NOT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#values of x and y will be take from the previous cell\n",
    "#Relational operator\n",
    "print(\"Is x greater than y? \", x>y)\n",
    "print(\"Is x equal to y? \", x==y)\n",
    "print(\"Is x less than y? \", x<y)\n",
    "\n",
    "#Logical operator\n",
    "z = 1.0\n",
    "print(\"Are both the condition satisfied? \", (x==z and x==y))\n",
    "print(\"Is one of the condition satisfied? \", (x==z or x==y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Decision structures\n",
    "**Decision structures** are used to evaluate variables, check for conditions and take actions according to the defined condition.\n",
    "`Python` comes with the following structures of decision making:\n",
    "\n",
    "- **if** statements\n",
    "- **if...else** statements\n",
    "- nested if statements\n",
    "\n",
    "**The statements are executed sequentially.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 5\n",
    "y = 6\n",
    "if (x>=y):\n",
    "    print(\"x is greater than or equal to y\")\n",
    "else:\n",
    "    print(\"x is less than y\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loops\n",
    "\n",
    "In case you have to continue a process several times or until a certain condition is reached, these process can be executed inside a **loop**. `Python` comes with the following built-in types for loops:\n",
    "\n",
    "- **while** loop: repeats the statements inside of the loop until the given condition is true\n",
    "- **for** loop: executes the sequence of statements inside of the loop for a given times\n",
    "- nested loops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "count = 0\n",
    "while (count < 5):\n",
    "    count += 1 #increment the variable by one. Equivalent to count = count + 1\n",
    "    print(\"From while loop: \", count) #print the value\n",
    "\n",
    "list_x = range(1,5) #create a list from 1 untill 5 (5 will not be included)\n",
    "\n",
    "for i in list_x: #looping though each element in the variable 'list_x'\n",
    "    print(\"From for loop: \", i)\n",
    "\n",
    "#looping through multiple variables at the same tTimeoutError\n",
    "numbers = [1, 2, 3, 4, 5]\n",
    "words = [\"one\", \"two\", \"three\", \"four\", \"five\" ]\n",
    "\n",
    "for n, w in zip(numbers, words):\n",
    "    print(f\"{n} -> {w}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Functions\n",
    "Sometime a processes in a programs might have to be done multiple times. It is a good practice and saves a lot of time and resources to outsource these processes into **functions**. \n",
    "\n",
    "A **function** is a block of code that is designed to perform a single action and is reusable. To use functions in `Python` brings **modulatrity**  to the project. `Python` comes with a row of built-in functions (ex: `type()` and `print()`) but it's as well possible to write your own functions.\n",
    "\n",
    "### Function Syntax\n",
    "```python\n",
    "def function_name(argument_1, argument_2, argument_n):\n",
    "    # code that should be processed when the function is called\n",
    "    return result\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#function to perform square of a list variable\n",
    "def square(list_values):\n",
    "    ret_var = [] #initialize an empty list\n",
    "    for value in list_values:\n",
    "        ret_var.append(value**2) #squaring the value and adding it to the return variable\n",
    "    return ret_var\n",
    "\n",
    "#create a list first\n",
    "input_value = list(range(1,5)) #implicit conversion to list data-type\n",
    "print(\"Value before squaring: \", input_value)\n",
    "print(\"Value after squaring: \", square(input_value)) #squaring and printing the values in the same line\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modules\n",
    "In python (or in any other programming language) there are always libraries that have a set of **predefined functionalities (i.e functions)** that you can use to avoid redoing everything yourself. There are over 137,000 python libraries present today, and they play a vital role in developing machine learning, data science, data visualization, image and data manipulation applications, and more.\n",
    "\n",
    "**To use a library in your code we use the `import` keyword of python**. This will add the library to your code and will enable you to use all the functions present in the library.\n",
    "\n",
    "For example, one of the **most commonly used library is the `math` library**. This **contains all the basic math functionalities** you might need while working with numbers. For example, we just implemented a function called `square` to simply square the values of a list which uses `**2` to raise it to a power of two. We can also use a function called `pow` from the `math` library that will raise the value to any power. You can find the sqaure root of a value using the `sqrt` function.\n",
    "\n",
    "You find the list of all the available functions in the `math` library [here](https://docs.python.org/3/library/math.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math #importing the library\n",
    "\n",
    "a = 2\n",
    "\n",
    "#in the last print statement pay attention to how the cube root is computed using the pow() function\n",
    "print(\"Value: {} Square: {} Cube: {}\".format(a, math.pow(a,2), math.pow(a, 3)))\n",
    "print(\"Value: {} Square root: {} Cube root: {}\".format(a, math.sqrt(a), math.pow(a, 1/3)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**With this basics we will go through the course material and learn more python functionalities as we navigate the rest of the course.**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
