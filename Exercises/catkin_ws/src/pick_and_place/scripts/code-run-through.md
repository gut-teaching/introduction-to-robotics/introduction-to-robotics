# Run through of the `pick_place_example.py` code

The code is also explained as inline comments that will be helpful in understanding the code. Here we will give you an overview of each function.

## `main()` function:
This is the starting point of the code. 

The following lines will initialize the robot parameters and create an interface to the `move_it` package that is used to control the robot.

```python
rospy.loginfo("--- Starting initialising a robotics node ---")
    
    #Initializing movit API
    moveit_commander.roscpp_initialize(sys.argv)

    #Initializing the ROS node
    rospy.init_node('one_object_manipulation', anonymous=True) 
    
    #Intializing robot params
    scene, robot_commander, gripper, arm = initialize_robot_params()

    # Getting some robot details
    rospy.logwarn("--- Fetching Information about the robot ---")
    planning_frame = arm.get_planning_frame()
    eef_frame = arm.get_end_effector_link()
  
    rospy.loginfo('Planning frame: '+ planning_frame)
    rospy.loginfo("End effector link: "+ eef_frame)
```

Then we remove the objects from the world. The `remove_world_object` function removes objects from the scene. If the input argument is empty it removes all objects. You can also input an `object_id` (or name) as an input to delete that particular object from the scene.

```python
    scene.remove_world_object()
```

Then the main function calls [add_itmes](#add_itemsscene-frame-function), [gripper_action](#gripper_actiongripper_end-actionopen-function), [move_to_pose](#move_to_posearm-goal_pose-function) function to demonstrate the functionalities.


## `add_items(scene, frame)` function:
This function takes the sceneInterface and a reference frame as arguments.

The function call another function called `create_object`. This function will return a `CollisionObject()` variable that we will then add to the world.

The `add_object` function will add the collision object returned by the `create_object` to the `scene`

```python
#Add Objects to the scene
scene.add_object(table1)
scene.add_object(table2)
scene.add_object(box1)
```

## `create_object(obj_id, ref_frame, dimensions, pose)` function:
The following lines define the name of the object using `obj_id`varaible and the `ref_frame` in which the object will be added. 

```python
 object.id = obj_id
 object.header.frame_id = ref_frame
```
We can define the object dimension using the `dimensions` variable. The format of the `dimensions` variable is `[widht, breadth, thickness]` in meters

```python
solid = SolidPrimitive()
solid.type = solid.BOX
solid.dimensions = dimensions
object.primitives = [solid]
```

Then we define the position and orientation of the object using data from the `pose` varaible that is an input to the `create_object` function. The `pose` variable has the format `[x, y, z, roll, pitch, yaw]`.

```python
object_pose = Pose()
object_pose.position.x = pose[0]
object_pose.position.y = pose[1]
object_pose.position.z = pose[2]

#creating quaternion from (roll, pitch, yaw)
orientation_ = tf.transformations.quaternion_from_euler(pose[3], pose[4], pose[5])

object_pose.orientation.x = orientation_[0]
object_pose.orientation.y = orientation_[1]
object_pose.orientation.z = orientation_[2]
object_pose.orientation.w = orientation_[3]
```

Then we define the action that should be done for this collision object.
```python
object.operation = object.ADD
```

## `gripper_action(gripper_end, action="open")` function:

This function will move the `gripper_end` which defines the links in the gripper. The `move()` function takes the distance between the center of the gripper and the left finger. The max distance is given by the `max_bound()` function. The **max value that the gripper finger can move is 4 cm or 0.04 m**. The `action` argument defines the action we want to perform. In the close condition we move the gripper fingers to **50%** of th max distance.

```python
if(action == "open"):
    gripper_end.move(gripper_end.max_bound(), True)
elif(action == "close"):
    gripper_end.move(gripper_end.max_bound()*0.5, True)
```

The first argument of the `move()` function takes the distance between the center of the gripper and the left finger. The max value is **4 cm**. **Remember that you will have to later update this function (for task 1b) to take the object size into account and move the gripper accordingly.**

```
        _______________________
         |   |           |   |
         |   |           |   |
         |   |<-d->      |   |      d = 4 cm
         |   |     *     |   |
left --> |   |     ^     |   |
finger   |___|     |     |___|
                   |
                center of gripper
```

## `move_to_pose(arm, goal_pose)` function:

This function will move the end_effector to the position mentioned by the `goal_pose` argument. The `goal_pose` variable is a special type called `Pose`. You can find the message definition [here](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/Pose.html).

The planner will create a plan for the panda_link8 to reach the goal pose. But since we have an end-endeffector attached to it, it will cause collision. So we add the `wrist_to_tcp` position to the `z` position of the `goal_pose`.

```python
#defining pre-grasp position
grasp_pose = Pose()
grasp_pose.position.x = goal_pose.position.x
grasp_pose.position.y = goal_pose.position.y
#adding tcp distance because the planning frame is panda_link8
grasp_pose.position.z = goal_pose.position.z + wrist_to_tcp  
```

There is a orientation difference between the base frame and the tcp which is constant. You can change these values to see how the pick orientation varies.

```python
#Orientation of the tcp(panda_hand_tcp) w.r.t base frame(panda_link0) 
quaternion = tf.transformations.quaternion_from_euler(math.radians(180), 0, math.radians(-45))

gripper_angle.x = quaternion[0]
gripper_angle.y = quaternion[1]
gripper_angle.z = quaternion[2]
gripper_angle.w = quaternion[3]
```

Then we set the target position and start the motion of the arm.

```python
arm.set_pose_target(grasp_pose) #setting the pose of the end-effector

plan = arm.go(wait=True) #move the arm to the grasp_pose
```