#!/usr/bin/env python

# Task 2a - Autonomous Mapping

import rospy 
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist 

def callback(dt):
    '''
    Callback function for the subscriber to laserscan

    Args:
        dt - Laserscan data from the robot
    
    Return:
        None
    '''
    print ('-------------------------------------------')
    print ('Range data at 0 deg:   {}'.format(dt.ranges[0]))
    print ('Range data at 15 deg:  {}'.format(dt.ranges[15]))
    print ('Range data at 345 deg: {}'.format(dt.ranges[345]))
    print ('-------------------------------------------')

    ## YOUR TASK
    #Update the logic below to move the robot close to the wall and do wall following.
    
    thr1 = 0.8 # Laser scan range threshold
    thr2 = 0.8
    # Checks if there are obstacles in front(0) and 15 degrees left(index - 15) and right (index - 345)
    # Try changing the angle values as well as the thresholds
    if dt.ranges[0]>thr1 and dt.ranges[15]>thr2 and dt.ranges[345]>thr2: 
        move.linear.x = 0.5 # go forward (linear velocity)
        move.angular.z = 0.0 # do not rotate (angular velocity)
    else:
        move.linear.x = 0.0 # No forward motion
        move.angular.z = 0.5 # rotate counter-clockwise
        if dt.ranges[0]>thr1 and dt.ranges[15]>thr2 and dt.ranges[345]>thr2:
            move.linear.x = 0.5
            move.angular.z = 0.0
    pub.publish(move) # publish the move object

def main():
    global move, pub
    move = Twist() # Creates a Twist message type object
    
    rospy.init_node('obstacle_avoidance_node') # Initializes a node

    # Publisher object which will publish "Twist" type messages
    # on the "/cmd_vel" Topic, "queue_size" is the size of the
    # outgoing message queue used for asynchronous publishing
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=10)  

    # Subscriber object which will listen "LaserScan" type messages
    # from the "/scan" Topic and call the "callback" function
    # each time it reads something from the Topic
    sub = rospy.Subscriber("/scan", LaserScan, callback)  

    rospy.spin() # Loops infinitely until someone stops the program execution

if __name__ == "__main__":
    main()